// console.log("Hello World!");

/*[SECTION] Javascript Synchronous vs. Asynchronous
Synchronous = JV reads from top - bottom, left - right
Asynchronous = */

console.log("Hello World");
// conosle.log("Hello Again!"); = conosle is not defined
console.log("Goodbye");


console.log("Hello World");
// for(let i = 0; i <= 1500; i++){
// 	console.log(i);
// }
console.log("Hello Again!");


/*[SECTION] Getting all post
.fetch() - fetch request
The fetch API allows programmers to asynchronously request for a resource or data
promises are like objects, sometimes not fulfilled/rejected
SYNTAX: fetch(API)
console.log(fetch('https://jsonplaceholder.typicode.com/posts'))
*/


fetch("https://jsonplaceholder.typicode.com/posts")
// we use "json" method from the response object convert data into JSON format
.then((response) => response.json())
.then((json) => console.log(json));


// The "async" and "await" keywords are another approach that can be used to achive asynchronous
// Used in function to indicate which portions of the code should be awaited for.
// Creates an asynchronous function

async function fetchData(){
	// waits for the "fetch" method to complete then stores the value in the "result" variable
	let result = await fetch("https://jsonplaceholder.typicode.com/posts");
	// result 
	console.log(result);
	console.log(typeof result);
	console.log(result.body);

	let json = await result.json();
	console.log(json);
}
fetchData();


// [SECTION] Getting a specific post - it's where async happened
fetch("https://jsonplaceholder.typicode.com/posts/1")
.then((response) => response.json())
.then((json) => console.log(json));

// the lighter data loaded before the heavy data that needs to be loaded - asynchronous
// get the data in the postman
// 


// [SECTION] Creating a post 
/*
SYNTAX: fetch("URL", options)
.then((response) => {})
.then((response) => {})
*/

fetch("https://jsonplaceholder.typicode.com/posts", {
	method: "POST", 
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "New Post",
		body: "Hello World",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// [SECTION] UPDATING A POST
// PUT METHOD - is used to update a specific data
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		id: 1,
		title: "Updated Post",
		body: "Hello Again!",
		userId: 1
	})

})
.then((response) => response.json())
.then((json) => console.log(json));

// POSTMAN = body > raw > JSON


// [SECTION] Patch Method 
// update a specific post, specific property in a post
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Corrected Post"
	})

})
.then((response) => response.json())
.then((json) => console.log(json));


// PATCH - used to update a single/several properties
// PUT - used to update the whole document


// [SECTION] Deleting a post

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "DELETE"
});
// no changes in the browser 
// automatically data will be deleted in the postman, no indicator because we don't own the data 


// [SECTION] Filtering Post
/*
SYNTAX: 
if individual parameter:
'URL?parameterName=value' 
if multiple pa:
'URL?paramA=valueA&paramB=valueB'
*/

fetch("https://jsonplaceholder.typicode.com/posts?userId=2")
.then((response) => response.json())
.then((json) => console.log(json));


// [SECTION] Retrieving comments on a specific post
// post/ID/comments
fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
.then((response) => response.json())
.then((json) => console.log(json));






